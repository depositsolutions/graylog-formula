describe user('graylog') do
  it { should exist }
end

describe group('graylog') do
  it { should exist }
  its('gid') { should be < 1000 }
end

describe file('/etc/graylog/server') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file(' /etc/graylog/server/server.conf') do
 it { should exist }
end


describe service('graylog-server') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
