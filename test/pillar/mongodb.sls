mongodb:
  use_percona: True
  use_ldap_auth: True
  version: stable
  log_path: /srv/mongodb/log
  db_path: /srv/mongodb/data
  user_properties:
    optional_groups:
      - ssl-cert
  replicaset:
    key: minharola
    keyfile: &keyfile /srv/mongodb/keyfile
  mongod_settings:
    systemLog:
      destination: file
      logAppend: true
      path: /var/log/mongodb/mongod.log
    storage:
      dbPath: /srv/mongodb/data
      journal:
        enabled: true
    net:
      port: 27017
      bindIp: 0.0.0.0
    auditLog:
      destination: file
      format: JSON
      path: /var/log/mongodb/audit.json
    setParameter:
      enableLocalhostAuthBypass: true
      authenticationMechanisms: PLAIN,SCRAM-SHA-1
    security:
      authorization: enabled
      keyFile: *keyfile
    replication:
      replSetName: rs1
  users:
    admin:
      - check: False
      - passwd: passwd
      - roles:
        - db: admin
          role: root
    mongo-backup:
      - passwd: passwd
      - roles:
        - db: admin
          role: backup
    gluser:
      - passwd: password
      - database: graylog
      - roles:
        - role: readWrite
          db: graylog
